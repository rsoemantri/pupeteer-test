const express = require("express");
const puppeteer = require("puppeteer");
const cors = require('cors')
const app = express();
const PORT = 3000;

app.use(cors());

const generatePdf = async () => {
  // console.time('generating-pdf');
  // const browser = await puppeteer.launch();
  // const page = await browser.newPage();
  // console.log('pupeteer intialized.')
  // console.log('signing in to hi dashboard . . .')
  // await page.goto("http://localhost:4200/sign-in", {
  //   waitUntil: "networkidle2",
  // });
  // await page.type("#email", "smilingelsa771@gmail.com");
  // await page.type("#password", "123456");
  // // click and wait for navigation
  // await Promise.all([
  //   page.click("#submitBtn"),
  //   page.waitForNavigation({
  //     waitUntil: "networkidle0",
  //   }),
  // ]);

  // console.log('sign in success.')
  // console.log('navigating to invoice preview . . .')
  // await page.goto("http://localhost:4200/subscription/subscription-history/invoice", {
  //   waitUntil: "networkidle2",
  // });
  // console.log('navigate success.')
  // console.log('generating pdf . . .')

  // const pdf = await page.pdf({
  //   format: "a4",
  // });

  // console.log('pdf generated.')

  // await browser.close();
  // console.log('sending pdf.')
  // console.timeEnd('generating-pdf')
  // return pdf;

  (async () => {
    console.time('generating-pdf');
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://news.ycombinator.com', {
      waitUntil: 'networkidle2',
    });
    const resultPDf = await page.pdf({ format: 'a4'});

    console.log(resultPDf);
  
    await browser.close();

    console.timeEnd('generating-pdf');
    return resultPDf;
  })();
}


const captureGmaps = () => {
  (async () => {
    console.time('maps-start');
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({
      width: 800,
      height: 1000,
    });

    await page.goto('https://www.google.com/maps', {
      waitUntil: "networkidle2",
    });

    await page.type("#gs_taif50", "-6.912845740678788, 107.59550821948993");
    await Promise.all([
      page.click("#searchbox-searchbutton"),
      page.waitForNavigation({
        waitUntil: "networkidle0",
      }),
    ]);

    await page.screenshot({
      path: 'example.png',
    });

    await browser.close();
    console.timeEnd('maps-start');
  })();

}

app.get('/generate-pdf', (req, res) => {
  console.log('called')
  generatePdf().then((pdf) => {
    res.set({
      'Content-Type': 'application/pdf',
      'Content-Length': pdf.length
    })
    res.send(pdf)
  })
})

app.get('/gmaps', (req, res) => {
  res.status(200).json({
    message:'request received'
  })
  captureGmaps();
})

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});